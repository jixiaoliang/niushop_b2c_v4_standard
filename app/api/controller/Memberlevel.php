<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 上海牛之云网络科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com

 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\api\controller;

use app\model\member\MemberLevel as MemberLevelModel;

class Memberlevel extends BaseApi
{
    /**
     * 列表信息
     */
    public function lists()
    {
        $type = $this->params['type'] ?? 0;
        $member_level_model = new MemberLevelModel();

        $condition = [
            ['site_id', '=', $this->site_id],
            ['level_type', '=', $type]
        ];
        $field = 'level_id,level_name,growth,remark,consume_discount,is_free_shipping,point_feedback,send_point,send_balance,send_coupon,charge_rule,charge_tyep,bg_color';
        $member_level_list  = $member_level_model->getMemberLevelList($condition, $field, 'growth asc,level_id desc');
        return $this->response($member_level_list);
    }
}